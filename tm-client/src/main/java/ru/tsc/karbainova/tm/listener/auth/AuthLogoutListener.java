package ru.tsc.karbainova.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

public class AuthLogoutListener extends AbstractSystemListener {
    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout app";
    }

    @Override
    @EventListener(condition = "@authLogoutListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("Logout app");
        SessionDTO session = sessionService.getSession();

        sessionEndpoint.closeSession(session);
    }
}
